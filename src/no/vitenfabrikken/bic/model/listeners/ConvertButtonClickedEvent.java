package no.vitenfabrikken.bic.model.listeners;

public class ConvertButtonClickedEvent {
    private String selectedFormat;
    private boolean startedConverting;

    public ConvertButtonClickedEvent(boolean startedConverting, String selectedFormat) {
        this.selectedFormat = selectedFormat;
        this.startedConverting = startedConverting;
    }

    public String getSelectedFormat() {
        return selectedFormat;
    }

    public boolean isStartedConverting() {
        return startedConverting;
    }
}
