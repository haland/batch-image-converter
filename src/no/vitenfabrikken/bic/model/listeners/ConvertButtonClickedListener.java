package no.vitenfabrikken.bic.model.listeners;

public interface ConvertButtonClickedListener {
    public abstract void convertButtonClicked(ConvertButtonClickedEvent e);
}
