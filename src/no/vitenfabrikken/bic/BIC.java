package no.vitenfabrikken.bic;

import no.vitenfabrikken.bic.controller.Converter;
import no.vitenfabrikken.bic.view.Window;

import javax.swing.*;

public class BIC {
    private static BIC instance;

    public BIC() {
        Window window = new Window();

        new Converter(window, window.getInputPanel(), window.getOutputPanel(),
                window.getConvertPanel());
    }

    public static BIC getInstance() {
        if (instance == null) {
            instance = new BIC();
        }

        return instance;
    }

    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                e.printStackTrace();
            }
        });

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        getInstance();
    }
}
