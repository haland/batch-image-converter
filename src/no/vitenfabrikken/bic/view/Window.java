package no.vitenfabrikken.bic.view;

import no.vitenfabrikken.bic.view.panels.ConvertPanel;
import no.vitenfabrikken.bic.view.panels.ProgressPanel;
import no.vitenfabrikken.bic.view.layout.SpringUtilities;
import no.vitenfabrikken.bic.view.panels.InputPanel;
import no.vitenfabrikken.bic.view.panels.OutputPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class Window extends JFrame implements WindowListener {
    private static final Image ICON = new ImageIcon("images/logo.png").getImage();

    private JPanel panel;
    private InputPanel inputPanel;
    private OutputPanel outputPanel;
    private ProgressPanel progressPanel;
    private ConvertPanel convertPanel;

    public Window() {
        super("Batch Image Converter");

        inputPanel = new InputPanel();
        outputPanel = new OutputPanel();
        convertPanel = new ConvertPanel();

        panel = new JPanel(new SpringLayout());
        panel.add(inputPanel);
        panel.add(outputPanel);
        panel.add(convertPanel);

        SpringUtilities.makeCompactGrid(panel, panel.getComponentCount(), 1, 5, 5, 5, 5);

        add(panel);

        pack();
        setSize(460, getHeight());

        setPosition();
        setVisible(true);
        setIconImage(ICON);
        addWindowListener(this);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    public void setProgressBarProgress(String s) {
        if (progressPanel != null) {
            progressPanel.setProgress(s);
        }
    }

    public void setProgressBarPercent(int i) {
        if (progressPanel != null) {
            progressPanel.setPercent(i);
        }
    }

    public void addProgressBar(int maxValue) {
        progressPanel = new ProgressPanel(maxValue);
        panel.add(progressPanel);

        SpringUtilities.makeCompactGrid(panel, panel.getComponentCount(), 1, 5, 5, 5, 5);
        validate();

        pack();
        setSize(460, getHeight());
    }

    public void removeProgressBar() {
        panel.remove(progressPanel);
        progressPanel = null;

        SpringUtilities.makeCompactGrid(panel, panel.getComponentCount(), 1, 5, 5, 5, 5);
        validate();

        pack();
        setSize(460, getHeight());
    }

    public InputPanel getInputPanel() {
        return inputPanel;
    }

    public OutputPanel getOutputPanel() {
        return outputPanel;
    }

    public ConvertPanel getConvertPanel() {
        return convertPanel;
    }

    private void setPosition() {
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

        int xPos = screen.width / 2 - getWidth() / 2;
        int yPos = screen.height / 2 - getHeight() / 2;

        setLocation(xPos, yPos);
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowOpened(WindowEvent e) {}

    @Override
    public void windowClosed(WindowEvent e) {}

    @Override
    public void windowIconified(WindowEvent e) {}

    @Override
    public void windowActivated(WindowEvent e) {}

    @Override
    public void windowDeiconified(WindowEvent e) {}

    @Override
    public void windowDeactivated(WindowEvent e) {}
}
