package no.vitenfabrikken.bic.view.panels;

import no.vitenfabrikken.bic.view.layout.SpringUtilities;

import javax.swing.*;

public class ProgressPanel extends JPanel {
    private int maxValue;
    private JLabel progressLabel;
    private JProgressBar progressBar;

    public ProgressPanel(int maxValue) {
        this.maxValue = maxValue;

        setLayout(new SpringLayout());

        progressLabel = new JLabel("Preparing images...");
        progressBar = new JProgressBar(0, maxValue);

        add(progressLabel);
        add(progressBar);

        SpringUtilities.makeCompactGrid(this, getComponentCount(), 1, 5, 5, 5, 5);
    }

    public void setProgress(String name) {
        progressLabel.setText(name);
    }

    public void setPercent(int percent) {
        progressBar.setValue(percent);

        if (progressBar.getValue() >= maxValue) {
            setProgress("Finished!");
        }
    }
}
