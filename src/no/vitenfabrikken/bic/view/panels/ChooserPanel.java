package no.vitenfabrikken.bic.view.panels;

import no.vitenfabrikken.bic.view.chooser.JFolderChooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChooserPanel extends JPanel implements ActionListener {
    private JLabel label;
    private JButton button;
    private JTextField textField;
    private JFileChooser folderChooser;

    public ChooserPanel(String descriptionText) {
        super(new BorderLayout());
        folderChooser = new JFolderChooser(descriptionText);

        label = new JLabel(descriptionText);
        textField = new JTextField(folderChooser.getCurrentDirectory().getAbsolutePath());

        button = new JButton("Select...");
        button.addActionListener(this);

        add(label, BorderLayout.NORTH);
        add(textField, BorderLayout.CENTER);
        add(button, BorderLayout.EAST);
    }

    public void lock() {
        button.setEnabled(false);
        textField.setEnabled(false);
    }

    public void unlock() {
        button.setEnabled(true);
        textField.setEnabled(true);
    }

    public String getChosenFolder() {
        return textField.getText();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(button)) {
            if (folderChooser.showOpenDialog(new JDialog()) == JFileChooser.APPROVE_OPTION) {
                textField.setText(folderChooser.getSelectedFile().getAbsolutePath());
            }
        }
    }
}
