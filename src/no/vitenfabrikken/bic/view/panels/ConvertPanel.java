package no.vitenfabrikken.bic.view.panels;

import no.vitenfabrikken.bic.model.listeners.ConvertButtonClickedEvent;
import no.vitenfabrikken.bic.model.listeners.ConvertButtonClickedListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Vector;

public class ConvertPanel extends JPanel implements ActionListener {
    private static final String[] SUPPORTED_FORMATS = {"JPEG", "PNG"};

    private JLabel dropDownLabel;
    private String selectedFormat;
    private JButton convertButton;
    private JComboBox<String> dropDownList;
    private Vector<ConvertButtonClickedListener> listeners;

    public ConvertPanel() {
        super(new BorderLayout());
        listeners = new Vector<ConvertButtonClickedListener>();

        dropDownLabel = new JLabel("Select format to convert images into:");

        dropDownList = new JComboBox<String>(SUPPORTED_FORMATS);
        dropDownList.addActionListener(this);

        convertButton = new JButton("Start conversion process");
        convertButton.addActionListener(this);

        add(dropDownLabel, BorderLayout.NORTH);
        add(dropDownList, BorderLayout.CENTER);
        add(convertButton, BorderLayout.EAST);

        selectedFormat = ((String)dropDownList.getSelectedItem()).toLowerCase();
    }

    public String getSelectedFormat() {
        return selectedFormat;
    }

    public void addConvertButtonClickedListener(ConvertButtonClickedListener l) {
        if (!listeners.contains(l)) {
            listeners.add(l);
        }
    }

    public void removeConvertButtonClickedListener(ConvertButtonClickedListener l) {
        if (listeners.contains(l)) {
            listeners.remove(l);
        }
    }

    private void informListeners(ConvertButtonClickedEvent e) {
        Iterator<ConvertButtonClickedListener> i = listeners.iterator();
        while(i.hasNext()) {
            i.next().convertButtonClicked(e);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(dropDownList)) {
            selectedFormat = ((String)dropDownList.getSelectedItem()).toLowerCase();
        } else if (e.getSource().equals(convertButton)) {
            if (convertButton.getText().equals("Start conversion process")) {
                convertButton.setText("Stop conversion process");
                informListeners(new ConvertButtonClickedEvent(true, selectedFormat));
            } else {
                convertButton.setText("Start conversion process");
                informListeners(new ConvertButtonClickedEvent(false, null));
            }
        }
    }
}
