package no.vitenfabrikken.bic.view.panels;

public class OutputPanel extends ChooserPanel {
    public OutputPanel() {
        super("Select folder to store converted images:");
    }
}
