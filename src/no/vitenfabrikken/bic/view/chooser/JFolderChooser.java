package no.vitenfabrikken.bic.view.chooser;

import javax.swing.*;

public class JFolderChooser extends JFileChooser {
    public JFolderChooser(String dialogTitle) {
        super();

        setDialogTitle(dialogTitle);
        setAcceptAllFileFilterUsed(false);
        setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }
}
