package no.vitenfabrikken.bic.controller;

import no.vitenfabrikken.bic.model.BufferedTGAImage;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Stack;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Chris Håland <chris.haland@jaermuseet.no>
 *
 * Arguments must be:
 * 	2. Image name (discarding the last index numbers (e.g. 00000)
 * 	3. Number of images
 */

@Deprecated
public class ImageConverter implements Runnable {
    public final static boolean DEBUG = true;

    private static Logger log;
    private static FileHandler handler;

    public static void main(String[] args) {
        try {
            handler = new FileHandler("log.log", true);
            handler.setFormatter(new SimpleFormatter());
            log = Logger.getLogger("");
            log.addHandler(handler);
        } catch (SecurityException e) {
            if (DEBUG) e.printStackTrace();
        } catch (IOException e) {
            if (DEBUG) e.printStackTrace();
        } new ImageConverter(args[0], Integer.parseInt(args[1]));
    }

    private int count;
    private int current;
    private JFrame frame;
    private JPanel panel;
    private JLabel label;
    private Thread thread;

    public ImageConverter(String start, int count) {
        current = 0;
        this.count = count;

        frame = new JFrame("Full Dome TGA to JPG Converter");
        panel = new JPanel(new BorderLayout());
        label = new JLabel("0%");
        panel.add(label, BorderLayout.CENTER);
        frame.getContentPane().add(panel);

        frame.setVisible(true);
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        thread = new Thread(this);
        thread.start();

        stack = new Stack<String>();


        convertImage(start, count);
    }

    private Stack<String> stack;
    private void convertImage(String start, int count) {
        for (int a = 0; a < count; a++) {
            String path = "/" + start + stack.pop();
            BufferedImage image = null;

            try {
                image = BufferedTGAImage.getImage(path);
            } catch (IOException e) {
                if (DEBUG) e.printStackTrace();
                System.exit(0);
            }

            path = path.replace("tga", "jpg");
            File file = new File(path);

            try {
                ImageIO.write(image, path, file);
            } catch (IOException e) {
                if (DEBUG) e.printStackTrace();
            }
        }

    }

    public void run() {
        label.setText(String.valueOf(current/count));
        panel.validate();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            if (DEBUG) e.printStackTrace();
        }
    }
}