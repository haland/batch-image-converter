package no.vitenfabrikken.bic.controller;

import no.vitenfabrikken.bic.model.listeners.ConvertButtonClickedEvent;
import no.vitenfabrikken.bic.model.listeners.ConvertButtonClickedListener;
import no.vitenfabrikken.bic.view.Window;
import no.vitenfabrikken.bic.view.panels.ConvertPanel;
import no.vitenfabrikken.bic.view.panels.InputPanel;
import no.vitenfabrikken.bic.view.panels.OutputPanel;

import javax.media.jai.JAI;
import javax.swing.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Converter implements ConvertButtonClickedListener {
    public static final String[] SUPPORTED_FILE_TYPES = { "jpg", "jpeg", "png", "bmp", "wbmp", "tga" };

    private boolean abort;

    private Window window;
    private InputPanel input;
    private OutputPanel output;
    private Semaphore semaphore;

    public Converter(Window window, InputPanel input, OutputPanel output, ConvertPanel convertPanel) {
        this.input = input;
        this.output = output;
        this.window = window;

        abort = false;
        semaphore = new Semaphore(1);
        convertPanel.addConvertButtonClickedListener(this);
    }

    public void convert(File srcFolder, File dstFolder, String dstFileType)
            throws IllegalArgumentException {

        if (!isFileTypeSupported(dstFileType)) {
            throw new IllegalArgumentException("Source or destination file type is not supported.");
        }

        if (!srcFolder.isDirectory() || !dstFolder.isDirectory()) {
            throw new IllegalArgumentException("Source or destination folder defined is not an existing directory");
        }

        File[] srcFiles = srcFolder.listFiles();
        LinkedList<File> imageFileQueue = new LinkedList<File>();

        for (int i = srcFiles.length-1; i >= 0; i--) {
            String srcPath = srcFiles[i].getAbsolutePath();

            String[] srcSplitPath = srcPath.split("\\.");
            String srcFileType = srcSplitPath[srcSplitPath.length-1];

            if (!srcFiles[i].isDirectory() && isFileTypeSupported(srcFileType)) {
                imageFileQueue.push(srcFiles[i]);
            }
        }

        new ConvertImages(imageFileQueue, dstFolder, dstFileType).execute();
    }

    private static boolean isFileTypeSupported(String fileType) {
        boolean isSupported = false;
        for (String supportedFileType : SUPPORTED_FILE_TYPES) {
            if (fileType.toLowerCase().equals(supportedFileType.toLowerCase())) {
                isSupported = true;
                break;
            }
        }

        return isSupported;
    }

    private static String getFileNameWithoutFileType(File f) {
        String[] pathSplit = f.getName().split("\\.");
        String fileName = "";

        for (int i = 0; i < pathSplit.length-1; i++) {
            fileName += pathSplit[i];
        }

        return  fileName;
    }

    @Override
    public void convertButtonClicked(ConvertButtonClickedEvent e) {
        try {

            if (e.isStartedConverting()) {
                abort = false;

                File srcFolder = new File(input.getChosenFolder());
                File dstFolder = new File(output.getChosenFolder());
                String dstFileType = e.getSelectedFormat();

                window.addProgressBar(100);
                convert(srcFolder, dstFolder, dstFileType);
            } else {
                abort = true;
                window.setProgressBarProgress("Canceling...");
                semaphore.acquire();
                window.removeProgressBar();
                semaphore.release();
            }

        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    private class ConvertImages extends SwingWorker<String, Integer> {
        private File dstFolder;
        private String dstFileType;
        private LinkedList<File> imageQueue;

        private  ConvertImages(LinkedList<File> imageQueue, File dstFolder, String dstFileType) {
            this.imageQueue = imageQueue;
            this.dstFolder = dstFolder;
            this.dstFileType = dstFileType;
        }

        @Override
        protected String doInBackground() throws Exception {
            int count = 0;
            int numberOfFiles = imageQueue.size();
            String dstFolderPath = dstFolder.getAbsolutePath();

            while (!imageQueue.isEmpty()) {
                try {
                    semaphore.acquire();

                    if (abort) {
                        return null;
                    }

                    try {
                        File f = imageQueue.pop();
                        String imageFileName = getFileNameWithoutFileType(f);

                        count++;
                        window.setProgressBarProgress("Processing '" + f.getName() + "'...");

                        File dstFile = new File(dstFolderPath + File.separator + imageFileName + "." + dstFileType);
                        if (!dstFile.exists())
                            dstFile.createNewFile();

                        RenderedImage image = JAI.create("fileload", f.getAbsolutePath());
                        JAI.create("filestore", image, dstFile.getAbsolutePath(),
                                (dstFileType.equals(SUPPORTED_FILE_TYPES[0]) ? SUPPORTED_FILE_TYPES[1] : dstFileType));

                        publish((int) Math.floor((count + 1.0f) / numberOfFiles));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    semaphore.release();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void process(List<Integer> chunks) {
            for (Integer percent : chunks)
                window.setProgressBarPercent(percent);
            super.process(chunks);
        }
    }
}
